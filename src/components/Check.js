import { Row, Col, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Check({ checkProps }) {
  const { title, content, destination, label } = checkProps;

  return (
    <Row>
      <Col></Col>
      <Col>
        <div className="text-center">
          <h1 className="white-font font">{title}</h1>
          <p>{content}</p>
          <div>
            <Link to={destination}>
              <Button variant="warning" className="font-products">
                {label}
              </Button>
            </Link>
          </div>
        </div>
      </Col>
      <Col></Col>
    </Row>
  );
}
