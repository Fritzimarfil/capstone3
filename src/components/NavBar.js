import React, { useContext } from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import { Link, useNavigate, useLocation } from 'react-router-dom';
import UserContext from '../UserContext';
import logo from '../assets/images/logo.png';

export default function NavBar() {
  const { user, unsetUser } = useContext(UserContext);
  const navigate = useNavigate();
  const location = useLocation();

  // To clear localStorage and set userState to null
  const logout = () => {
    unsetUser();
    navigate('/login');
  };

  let rightNav = null;

  if (!user.id) {
    rightNav = (
      <>
        <Nav.Link as={Link} to="/register">
          Register
        </Nav.Link>
        <Nav.Link as={Link} to="/login">
          Log In
        </Nav.Link>
      </>
    );
  } else {
    if (user.isAdmin) {
      rightNav = (
        <>
          <Nav.Link as={Link} to="/adminView">
            Admin
          </Nav.Link>
          {location.pathname === '/adminView' && (
            <Nav.Link as={Link} to="/orders">
              Orders
            </Nav.Link>
          )}
          <Nav.Link onClick={logout}>Log Out</Nav.Link>
        </>
      );
    } else {
      rightNav = (
        <>
          <Nav.Link as={Link} to="/cart">
            Cart
          </Nav.Link>
          {user.isAdmin && (
            <Nav.Link as={Link} to="/orders">
              Orders
            </Nav.Link>
          )}
          <Nav.Link onClick={logout}>Log Out</Nav.Link>
        </>
      );
    }
  }

  return (
    <Navbar collapseOnSelect expand="lg" className="color-nav font">
      <Navbar.Brand as={Link} to="/">
        <img src={logo} height="50" alt="logo" />
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="responsive-navbar-nav" />
      <Navbar.Collapse id="responsive-navbar-nav">
        <Nav className="mx-auto">
          <Nav.Link as={Link} to="/">
            Home
          </Nav.Link>
          <Nav.Link as={Link} to="/products">
            Products
          </Nav.Link>
        </Nav>
        <Nav>{rightNav}</Nav>
      </Navbar.Collapse>
    </Navbar>
  );
}
