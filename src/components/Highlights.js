import { Row, Col, Card, Image } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Highlights(){
	return(
		<Row className="bottom-margin">
			<Col xs={12} md={3}>
				<Card className="card-highlight" bg="color-nav">
					<Card.Body>
						<Link to="/products/645e09790c1c0a0cae8904c4">
						<Image src="https://www.loomkart.com/wp-content/uploads/2019/12/PINK-BEDSHEET.jpg" fluid />
						</Link>
						<Card.Title className="App font-products black-font">Plain Pink</Card.Title>
					</Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={3}>
				<Card className="card-highlight" bg="gray-color">
					<Card.Body>
						<Link to="/products/645e0b260c1c0a0cae8904c9">
						<Image src="https://i.pinimg.com/originals/38/0c/d4/380cd48a625f34edf6f7bdd95ba943b5.jpg" fluid />
						</Link>
						<Card.Title className="App font-products black-font">Tribal Blue</Card.Title>
					</Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={3}>
				<Card className="card-highlight" bg="gray-color">
					<Card.Body>
						<Link to="/products/646228a1036a0d2ae3392564">
						<Image src="https://i.pinimg.com/originals/36/eb/cf/36ebcfb7f3de0c9d71c8ad4175462f47.jpg" fluid />
						</Link>
						<Card.Title className="App font-products black-font">Abstract Orange</Card.Title>
					</Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={3}>
				<Card className="card-highlight" bg="gray-color">
					<Card.Body>
						<Link to="/products/646372b724a1ccfa77eee0de">
						<Image src="https://f1af951e8abcbc4c70b9-9997fa854afcb64e87870c0f4e867f1d.lmsin.net/1000006160104-1000006160103-17418_01-1420.jpg" fluid />
						</Link>
						<Card.Title className="App font-products black-font">Pink Circle</Card.Title>
					</Card.Body>
				</Card>
			</Col>
		</Row>
	)
}
