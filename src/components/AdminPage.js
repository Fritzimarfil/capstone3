import { useState, useEffect } from 'react';
import AdminView from './pages/AdminView';

export default function AdminPage() {
  const [productsData, setProductsData] = useState([]);
  const [products, setProducts] = useState([]);

  // Function to fetch products data
  const fetchData = () => {
    // Fetch logic here to retrieve the products data
    // Assign the fetched data to the productsData state
    // For example: setProductsData(fetchedData);
  };

  // Fetch data when the component mounts
  useEffect(() => {
    fetchData();
  }, []);

  useEffect(() => {
    if (productsData) {
      const productsArr = productsData.map(product => {
        // Perform mapping logic here
        // For example: return <div key={product.id}>{product.name}</div>;
      });

      setProducts(productsArr);
    }
  }, [productsData]);

  return (
    <div>
      <h1>Admin View</h1>
      <AdminView products={products} fetchData={fetchData} />
    </div>
  );
}
