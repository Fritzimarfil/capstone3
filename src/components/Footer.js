import React from 'react';
import { Row, Col, Card } from 'react-bootstrap';

export default function Footer() {
  return (
    <footer>
      <div className="footer">
        <div className="container-fluid">
          <Row>
            <Col xs={12} md={4}></Col>
            <Col xs={12} md={4} className="black-font">
              <Card.Text className="footerText App">Saint's Linen Ⓒ</Card.Text>
              <Card.Text className="copyright App">
                By: Fritzi Dixy Marfil 2023
              </Card.Text>
            </Col>
            <Col xs={12} md={4}></Col>
          </Row>
        </div>
      </div>
    </footer>
  );
}
