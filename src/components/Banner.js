import { Carousel } from 'react-bootstrap';

export default function Banner({ bannerProps }) {
  return (
    <Carousel className="bottom-margin">
      <Carousel.Item interval={1000}>
        <img
          className="d-block w-100"
          src="https://www.loomkart.com/wp-content/uploads/2019/12/PINK-BEDSHEET.jpg"
          alt="First slide"
          style={{ objectFit: 'contain', height: '100vh' }}
        />
      </Carousel.Item>
      <Carousel.Item interval={1000}>
        <img
          className="d-block w-100"
          src="https://i.pinimg.com/originals/38/0c/d4/380cd48a625f34edf6f7bdd95ba943b5.jpg"
          alt="Second slide"
          style={{ objectFit: 'contain', height: '100vh' }}
        />
      </Carousel.Item>
      <Carousel.Item interval={1000}>
        <img
          className="d-block w-100"
          src="https://i.pinimg.com/originals/36/eb/cf/36ebcfb7f3de0c9d71c8ad4175462f47.jpg"
          alt="Third slide"
          style={{ objectFit: 'contain', height: '100vh' }}
        />
      </Carousel.Item>
      <Carousel.Item interval={1000}>
        <img
          className="d-block w-100"
          src="https://f1af951e8abcbc4c70b9-9997fa854afcb64e87870c0f4e867f1d.lmsin.net/1000006160104-1000006160103-17418_01-1420.jpg"
          alt="Fourth slide"
          style={{ objectFit: 'contain', height: '100vh' }}
        />
      </Carousel.Item>
    </Carousel>
  );
}
