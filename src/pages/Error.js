import React from 'react';
import { Row, Col, Button, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Error() {
  const data = {
    title: "404 - Not found",
    content: "The page you are looking for cannot be found",
    destination: "/",
    label: "Back home"
  };

  return (
    <Row>
      <Col></Col>
      <Col>
        <div className="text-center">
          <h1 className="white-font font">{data.title}</h1>
          <Card.Text>{data.content}</Card.Text>
          <div>
            <Link to={data.destination}>
              <Button variant="warning" className="font-products">
                {data.label}
              </Button>
            </Link>
          </div>
        </div>
      </Col>
      <Col></Col>
    </Row>
  );
}
