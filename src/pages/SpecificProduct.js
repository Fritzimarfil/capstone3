import { useContext, useEffect, useState } from 'react';
import { Container, Card, Button, FormControl } from 'react-bootstrap';
import { Link, useParams, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';

import UserContext from '../UserContext';

export default function SpecificProduct() {
  const { user } = useContext(UserContext);
  const { productId } = useParams();
  const navigate = useNavigate();
  const [cart, setCart] = useState([]);
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState("");
  const [color, setColor] = useState("");
  const [quantity, setQuantity] = useState(1);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
      .then((res) => res.json())
      .then((data) => {
        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
        setColor(data.color);
      });
  }, [productId]);

  const addToCart = () => {
    let alreadyInCart = false;
    let productIndex;
    let cart = [];

    if (localStorage.getItem("cart")) {
      cart = JSON.parse(localStorage.getItem("cart"));
    }

    for (let i = 0; i < cart.length; i++) {
      if (cart[i].productId === productId) {
        alreadyInCart = true;
        productIndex = i;
      }
    }

    if (alreadyInCart) {
      cart[productIndex].quantity += quantity;
      cart[productIndex].subtotal = cart[productIndex].price * cart[productIndex].quantity;
      Swal.fire({
        title: "Added to cart!",
        icon: "success",
        text: "You have successfully added this product to your cart.",
        confirmButtonColor: "#FFB138",
      });
    } else {
      cart.push({
        productId: productId,
        name: name,
        price: price,
        quantity: quantity,
        subtotal: price * quantity,
      });
      Swal.fire({
        title: "Added to cart!",
        icon: "success",
        text: "You have successfully added this product to your cart.",
        confirmButtonColor: "#FFB138",
      });
    }

    localStorage.setItem("cart", JSON.stringify(cart));
  };

  const increment = () => {
    setQuantity((prevCount) => prevCount + 1);
  };

  const decrement = () => {
    if (quantity > 1) {
      setQuantity(quantity - 1);
    }
  };

  return (
    <Container>
      <Card className="mt-5">
        <Card.Header className="bg-info text-dark text-center pb-0 font">
          <h4>{name}</h4>
        </Card.Header>

        <Card.Body className="text-center">
          <Card.Text className="text-secondary">
            <h4>{description}</h4>
          </Card.Text>


          <Card.Text>Color: {color}</Card.Text>
          <Card.Text className="d-flex justify-content-center align-items-center mb-1">
            <span className="mr-2">Quantity:</span>
            <div className="input-group">
              <Button variant="outline-dark" type="button" onClick={decrement}>
                -
              </Button>

              <FormControl
                className="form-control text-center"
                min="1"
                type="number"
                value={quantity}
                onChange={(e) => setQuantity(parseInt(e.target.value))}
              />

              <Button variant="outline-dark" type="button" onClick={increment}>
                +
              </Button>
            </div>
          </Card.Text>
          <Card.Text as="h6">Price: Php {price}</Card.Text>
        </Card.Body>
        <Card.Footer className="d-flex justify-content-center">
          {user.id !== null ? (
            <Button variant="warning" onClick={addToCart}>
              Add to Cart
            </Button>
          ) : (
            <Link className="btn btn-danger" to="/login">
              Add to Cart
            </Link>
          )}
        </Card.Footer>
      </Card>
    </Container>
  );
}
