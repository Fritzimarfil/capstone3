import React, { useState, useEffect } from 'react';
import { Table, Button, Modal, Form, Row, Col } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function AdminView() {
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState('');
  const [color, setColor] = useState('');
  const [quantity, setQuantity] = useState('');
  const [products, setProducts] = useState([]);
  const [showEdit, setShowEdit] = useState(false);
  const [showAdd, setShowAdd] = useState(false);
  const [productId, setProductId] = useState('');
  const [isAdded, setIsAdded] = useState(false);

  const openAdd = () => setShowAdd(true);
  const closeAdd = () => setShowAdd(false);

  const openEdit = (productId) => {
    setProductId(productId);
    setShowEdit(true);
  };

  const closeEdit = () => {
    setShowEdit(false);
    setProductId('');
    setName('');
    setDescription('');
    setPrice('');
    setColor('');
    setQuantity('');
  };

  const fetchData = () => {
    fetch("${process.env.REACT_APP_API_URL}/products", {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      }
    })
      .then(res => res.json())
      .then(data => {
        setProducts(data);
      })
      .catch(error => {
        console.log(error);
      });
  };

  useEffect(() => {
    fetchData();
  }, [isAdded]); // Trigger fetchData when isAdded changes

  const addProduct = (e) => {
    e.preventDefault();

    fetch("${process.env.REACT_APP_API_URL}/products", {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        name: name,
        description: description,
        price: price,
        color: color,
        quantity: quantity
      })
    })
      .then(res => res.json())
      .then(data => {
        if (data === true) {
          setIsAdded(true); // Set isAdded to true
          Swal.fire({
            title: "Product successfully added",
            icon: "success",
            text: "Updated",
            confirmButtonColor: '#FFB138'
          });
          closeAdd();
        } else {
          setIsAdded(false); // Set isAdded to false
          Swal.fire({
            title: "Error",
            icon: "error",
            text: "Something went wrong.",
            confirmButtonColor: '#FFB138'
          });
          closeAdd();
        }
      });
  };

  const editProduct = async (e) => {
    e.preventDefault();

    try {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem('token')}`
        },
        body: JSON.stringify({
          name: name,
          description: description,
          price: price,
          color: color,
          quantity: quantity
        })
      });

      if (response.ok) {
        Swal.fire({
          title: "Product successfully updated",
          icon: "success",
          text: "Updated",
          confirmButtonColor: '#FFB138'
        }).then(() => {
          closeEdit();
          fetchData(); // Fetch updated product list after successful update
        });
      } else {
        throw new Error("Failed to update product");
      }
    } catch (error) {
      console.log(error);
      Swal.fire({
        title: "Error!",
        text: "Something went wrong.",
        icon: "error",
        confirmButtonColor: "#FFB138",
      });
    }
  };

  const archiveProduct = async (productId) => {
    try {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
        method: "PATCH",
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`,
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          isActive: false
        })
      });

      if (response.ok) {
        const updatedProducts = products.map(product => {
          if (product._id === productId) {
            return {
              ...product,
              isActive: false
            };
          }
          return product;
        });
        setProducts(updatedProducts);
        Swal.fire({
          title: "Archived!",
          text: "Product has been archived.",
          icon: "success",
          confirmButtonColor: "#FFB138"
        });
      } else {
        throw new Error("Failed to archive product");
      }
    } catch (error) {
      console.log("Error:", error);
      Swal.fire({
        title: "Error!",
        text: "Something went wrong.",
        icon: "error",
        confirmButtonColor: "#FFB138"
      });
    }
  };



  return (
    <div className="container text-center">
      <h1>Admin Dashboard</h1>
      <Button variant="primary" onClick={openAdd} className="mb-3">
        Add Product
      </Button>
      <Table striped bordered hover responsive>
        <thead>
          <tr>
            <th>Name</th>
            <th>Description</th>
            <th>Price</th>
            <th>Color</th>
            <th>Quantity</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {products.map(product => (
            <tr key={product._id}>
              <td>{product.name}</td>
              <td>{product.description}</td>
              <td>{product.price}</td>
              <td>{product.color}</td>
              <td>{product.quantity}</td>
              <td>
                <Button
                  variant="success"
                  onClick={() => openEdit(product._id)}
                >
                  Edit
                </Button>{' '}
                <Button
                  variant="danger"
                  onClick={() => archiveProduct(product._id)}
                >
                  Archive
                </Button>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>

      <Modal show={showAdd} onHide={closeAdd}>
        <Modal.Header closeButton>
          <Modal.Title>Add Product</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={addProduct}>
            <Form.Group controlId="formName">
              <Form.Label>Name</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter name"
                value={name}
                onChange={(e) => setName(e.target.value)}
              />
            </Form.Group>
            <Form.Group controlId="formDescription">
              <Form.Label>Description</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter description"
                value={description}
                onChange={(e) => setDescription(e.target.value)}
              />
            </Form.Group>
            <Form.Group controlId="formPrice">
              <Form.Label>Price</Form.Label>
              <Form.Control
                type="number"
                placeholder="Enter price"
                value={price}
                onChange={(e) => setPrice(e.target.value)}
              />
            </Form.Group>
            <Form.Group controlId="formColor">
              <Form.Label>Color</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter color"
                value={color}
                onChange={(e) => setColor(e.target.value)}
              />
            </Form.Group>
            <Form.Group controlId="formQuantity">
              <Form.Label>Quantity</Form.Label>
              <Form.Control
                type="number"
                placeholder="Enter quantity"
                value={quantity}
                onChange={(e) => setQuantity(e.target.value)}
              />
            </Form.Group>
            <Button variant="primary" type="submit">
              Add
            </Button>
          </Form>
        </Modal.Body>
      </Modal>

      <Modal show={showEdit} onHide={closeEdit}>
        <Modal.Header closeButton>
          <Modal.Title>Edit Product</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={editProduct}>
            <Form.Group controlId="formName">
              <Form.Label>Name</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter name"
                value={name}
                onChange={(e) => setName(e.target.value)}
              />
            </Form.Group>
            <Form.Group controlId="formDescription">
              <Form.Label>Description</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter description"
                value={description}
                onChange={(e) => setDescription(e.target.value)}
              />
            </Form.Group>
            <Form.Group controlId="formPrice">
              <Form.Label>Price</Form.Label>
              <Form.Control
                type="number"
                placeholder="Enter price"
                value={price}
                onChange={(e) => setPrice(e.target.value)}
              />
            </Form.Group>
            <Form.Group controlId="formColor">
              <Form.Label>Color</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter color"
                value={color}
                onChange={(e) => setColor(e.target.value)}
              />
            </Form.Group>
            <Form.Group controlId="formQuantity">
              <Form.Label>Quantity</Form.Label>
              <Form.Control
                type="number"
                placeholder="Enter quantity"
                value={quantity}
                onChange={(e) => setQuantity(e.target.value)}
              />
            </Form.Group>
            <Button variant="primary" type="submit">
              Update
            </Button>
          </Form>
        </Modal.Body>
      </Modal>
    </div>
  );
}
