import Banner from '../components/Banner';
import Check from '../components/Check';
import Highlights from '../components/Highlights';
// import { Container } from 'react-bootstrap';
import Footer from '../components/Footer';


export default function Home(){
	const data = {
		title: "Featured Products",
		destination: "/products",
		label: "Order now!"
	}

	return(
		<>
			<div className="whitesmoke-color">
				<Banner bannerProps={data} />	
				<Check checkProps={data} />
				<Highlights />
				<Footer />
			</div>
		</>
	)
}
