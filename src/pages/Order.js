import React, { useState, useEffect } from 'react';
import { Container, Card, Table, ResponsiveEmbed } from 'react-bootstrap';

export default function Order() {
  const [orderHistory, setOrderHistory] = useState([]);

  useEffect(() => {
    const token = localStorage.getItem('token');

    if (token) {
      fetch('${process.env.REACT_APP_API_URL}/users/orders', {
        headers: {
          Authorization: `Bearer ${token}`
        }
      })
        .then(res => res.json())
        .then(data => {
          if (Array.isArray(data)) {
            setOrderHistory(data);
          } else {
            console.error('Invalid order history data:', data);
          }
        })
        .catch(error => {
          console.error('Error fetching order history:', error);
        });
    }
  }, []);

  return (
    <Container>
      <Card className="bg-info text-white form-margin text-center">
        <Card.Header>My Order History</Card.Header>
        <Card.Body>
          {orderHistory.length === 0 ? (
            <p>No orders found.</p>
          ) : (
            <Table striped bordered hover responsive="sm" variant="info">
              <thead>
                <tr>
                  <th>Product</th>
                  <th>Quantity</th>
                  <th>Price</th>
                  <th>Purchased On</th>
                  <th>Shipping Fee</th>
                  <th>Total Amount</th>
                </tr>
              </thead>
              <tbody>
                {orderHistory.map(userOrder => (
                  <React.Fragment key={userOrder.userId}>
                    {userOrder.orders.map(order => (
                      <tr key={order._id}>
                        <td>{order.productId}</td>
                        <td>{order.quantity}</td>
                        <td>{order.price}</td>
                        <td>{order.orderedOn}</td>
                        <td>{order.shippingFee}</td>
                        <td>{order.totalAmount}</td>
                      </tr>
                    ))}
                  </React.Fragment>
                ))}
              </tbody>
            </Table>
          )}
        </Card.Body>
      </Card>
    </Container>
  );
}
