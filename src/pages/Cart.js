import { useState, useEffect, useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import { Container, Card, Button, Table, Row, Col } from 'react-bootstrap';
import UserView from "../components/UserView";
import UserContext from "../UserContext";
import Swal from 'sweetalert2';

export default function Cart() {
  const [cart, setCart] = useState([]);
  const [total, setTotal] = useState(0);
  const [quantity, setQuantity] = useState("");
  const { user } = useContext(UserContext);
  const navigate = useNavigate();

  // LocalStorage
  useEffect(() => {
    if (localStorage.getItem('cart')) {
      setCart(JSON.parse(localStorage.getItem('cart')));
    }
  }, []);

  const qtyInput = (productId, value) => {
    let tempCart = [...cart];
    for (let i = 0; i < tempCart.length; i++) {
      if (tempCart[i].productId === productId) {
        tempCart[i].quantity = parseFloat(value);
        tempCart[i].subtotal = tempCart[i].price * tempCart[i].quantity;
      }
    }
    setCart(tempCart);
    localStorage.setItem('cart', JSON.stringify(tempCart));
  };

  useEffect(() => {
    let tempTotal = 0;
    cart.forEach((item) => {
      tempTotal += item.subtotal;
    });
    setTotal(tempTotal);
  }, [cart]);

  const removeItem = (productId) => {
    let tempCart = [...cart];
    tempCart.splice(tempCart.findIndex(item => item.productId === productId), 1);
    setCart(tempCart);
    localStorage.setItem('cart', JSON.stringify(tempCart));
  };

  const order = () => {
    const checkoutCart = cart.map((item) => {
      return {
        productId: item.productId,
        quantity: item.quantity,
        price: item.price,
      };
    });

    fetch('${process.env.REACT_APP_API_URL}/users/checkout', {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        products: checkoutCart,
        totalAmount: total,
        shippingFee: 12
      })
    })
      .then(res => res.json())
      .then(data => {
        if (data === true) {
          Swal.fire({
            title: "Successfully ordered!",
            icon: 'success',
            text: "You have successfully ordered this product.",
            confirmButtonColor: '#FFB138'
          })
          navigate("/products");
        } else {
          Swal.fire({
            title: "Something went wrong",
            icon: "error",
            text: "Please try again.",
            confirmButtonColor: '#C5211C'
          })
        }
      })
      .catch(error => {
        // Handle the error here
        console.error(error);
        // Display an error message to the user or take appropriate action
        Swal.fire({
          title: "Something went wrong",
          icon: "error",
          text: "An error occurred while placing the order. Please try again.",
          confirmButtonColor: '#C5211C'
        })
      });
  };


  const cartArr = cart.map((product) => {
    return (
      <tr key={product.productId}>
        <td>{product.name}</td>
        <td>{product.price.toFixed(2)}</td>
        <td>
          <div className="qty mt-2 mb-1 input-group">
            <Button variant="outline-dark" type="button" onClick={() => qtyInput(product.productId, product.quantity - 1)}>-</Button>
            <input min="1" type="number" value={product.quantity} onChange={e => qtyInput(product.productId, e.target.value)}></input>
            <Button variant="outline-dark" type="button" onClick={() => qtyInput(product.productId, product.quantity + 1)}>+</Button>
          </div>
        </td>
        <td>{product.subtotal.toFixed(2)}</td>
        <td>
          <Button className="btn-danger" onClick={() => removeItem(product.productId)}>Remove</Button>
        </td>
      </tr>
    );
  });

  if (!user) {
    navigate("/");
    return null;
  }

  return (
    <Container className="py-4">
      <Row>
        <Col md={8}>
          <Card>
            <Card.Header>
              <h1>My Cart</h1>
            </Card.Header>
            <Card.Body>
              <Table responsive>
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th>Subtotal</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  {cartArr}
                </tbody>
              </Table>
            </Card.Body>
          </Card>
        </Col>
        <Col md={4}>
          <Card>
            <Card.Body>
              <Card.Title>Order Summary</Card.Title>
              <Card.Text>
                <strong>Total Amount: ₱{total.toFixed(2)}</strong>
              </Card.Text>
              <Button className="btn btn-primary" onClick={order}>Place Order</Button>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}
