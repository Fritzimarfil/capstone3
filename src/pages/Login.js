import { useState, useEffect, useContext } from 'react';
import { Form, Button, Container, Card, Row, Col } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Login() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const { user, setUser } = useContext(UserContext);
  const [logInButton, setlogInButton] = useState(false);
  const [isActive, setIsActive] = useState(true);

  function logInUser(e) {
    e.preventDefault();

    fetch('${process.env.REACT_APP_API_URL}/users/login', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (typeof data.access !== 'undefined') {
          localStorage.setItem('token', data.access);
          retrieveUserDetails(data.access);
        } else {
          Swal.fire({
            title: 'Authentication failed',
            icon: 'error',
            text: 'Check your login details and try again.',
            confirmButtonColor: '#C5211C',
          });
        }
      });
  }

  const retrieveUserDetails = (token) => {
    fetch('${process.env.REACT_APP_API_URL}/users/:users/userDetails', {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin,
        });
      });
  };

  if (user.id !== null) {
    if (user.isAdmin) {
      return <Navigate to="/adminView" />;
    } else {
      return <Navigate to="/" />;
    }
  }

  return (
    <Container>
      <Row className="justify-content-center form-margin">
        <Col xs={12} sm={8} md={6} lg={4}>
          <Card className="bg-info text-white">
            <Card.Body>
              <Form className="mt-3" onSubmit={(e) => logInUser(e)}>
                <Form.Group className="button-margin">
                  <Form.Label>Email Address:</Form.Label>
                  <Form.Control
                    type="email"
                    placeholder="Enter your email address"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                  />
                </Form.Group>

                <Form.Group>
                  <Form.Label>Password:</Form.Label>
                  <Form.Control
                    type="password"
                    placeholder="Enter your password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    required
                  />
                </Form.Group>

                <div className="row justify-content-center align-items-center button-margin">
                  {isActive ? (
                    <Button variant="warning" type="submit" id="submitBtn">
                      Submit
                    </Button>
                  ) : (
                    <Button variant="danger" type="submit" id="submitBtn" disabled>
                      Submit
                    </Button>
                  )}
                </div>
              </Form>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}
